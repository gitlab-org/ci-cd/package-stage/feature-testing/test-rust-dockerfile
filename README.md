# test-rust-dockerfile

This project is meant to help test https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28167, which adds a new `Dockerfile` template to GitLab for Rust.


Hello World web server in Rust.
```
$ docker run -d -p 8080:8080 registry.gitlab.com/gitlab-org/ci-cd/package-stage/feature-testing/test-rust-dockerfile
```

```
$ curl localhost:8080
Hello world
```
